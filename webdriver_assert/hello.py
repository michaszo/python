from selenium import webdriver


def add_task(driver, task):
    driver.get('http://localhost/NaukaPHP/PHPTODO/ToDoApp/index.php')
    new_task = driver.find_element_by_name('newTask')
    button = driver.find_element_by_xpath('/html/body/div[1]/form/button')
    new_task.send_keys(task)
    button.click()
    driver.close


driver = webdriver.Chrome(executable_path='C:/test_programs/chromedriver.exe')

arr = [
    'This text is send using Python code.',
    'Go jogging',
    'Tidy room',
    'Write inż'
]

for i in arr:
    add_task(driver, i)


