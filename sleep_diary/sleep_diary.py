import sqlite3
import subprocess as sp
from datetime import datetime, date, timedelta, time
from time import strftime, gmtime


def twenty_four_to_zero_zero(go_to_sleep_time):
    if go_to_sleep_time[:2] == '24':
        return f'00:{go_to_sleep_time[3:]}'
    return go_to_sleep_time


def create_table():
    conn = sqlite3.connect('sleep_diary.sqlite')
    cursor = conn.cursor()
    query = '''
	    CREATE TABLE IF NOT EXISTS diary(
	    	id INTEGER PRIMARY KEY, 
	    	date timestamp, 
	    	go_to_sleep_time text,
	    	wake_up_time text,
	    	first_alarm text,
	    	notes text
	    )
	'''
    cursor.execute(query)
    conn.commit()
    conn.close()


def add_data(go_to_sleep_time, wake_up_time, first_alarm='', notes='', date=date.today()):
    conn = sqlite3.connect('sleep_diary.sqlite')
    cursor = conn.cursor()
    query = "INSERT INTO diary(date, go_to_sleep_time, wake_up_time, first_alarm, notes) VALUES (?,?,?,?,?)"
    cursor.execute(query, (twenty_four_to_zero_zero(go_to_sleep_time), wake_up_time, first_alarm, notes, date))
    conn.commit()
    conn.close()


def get_data():
    conn = sqlite3.connect('sleep_diary.sqlite')
    cursor = conn.cursor()
    query = "SELECT * FROM diary"
    cursor.execute(query)
    all_rows = cursor.fetchall()
    conn.commit()
    conn.close()
    return all_rows


def get_sleep_by_id(id):
    conn = sqlite3.connect('sleep_diary.sqlite')
    cursor = conn.cursor()
    query = "SELECT * FROM diary WHERE id = (?)"
    cursor.execute(query, (id,))
    row = cursor.fetchone()
    conn.commit()
    conn.close()
    return row


def update_sleep(id, go_to_sleep_time='', wake_up_time='', first_alarm='', notes='', date=''):
    conn = sqlite3.connect('sleep_diary.sqlite')
    cursor = conn.cursor()
    dic = {
        'go_to_sleep_time': twenty_four_to_zero_zero(go_to_sleep_time),
        'wake_up_time': wake_up_time,
        'first_alarm': first_alarm,
        'notes': notes,
        'date': date
    }
    # query builder
    query = "UPDATE diary SET "
    for i, j in dic.items():
        print(i, j)
        if j != '':
            query += f"{i}=\"{j}\","
    query = query[:-1]
    query += f" WHERE id = {id}"
    cursor.execute(query)
    conn.commit()
    conn.close()


def delete_sleep(id):
    conn = sqlite3.connect('sleep_diary.sqlite')
    cursor = conn.cursor()
    query = "DELETE FROM diary WHERE id = (?)"
    cursor.execute(query, (id,))
    all_rows = cursor.fetchall()
    conn.commit()
    conn.close()
    return all_rows


def avg_sleep_time():
    fmt = '%H:%M'
    data = get_data()
    sleep_times = []
    for i in data:
        time_delta = datetime.strptime(i[2], fmt) - datetime.strptime(i[1], fmt)
        time_delta = str(time_delta).replace('-1 day, ', '')
        time_delta = time_delta.split(':')
        t = time(int(time_delta[0]), int(time_delta[1]))
        t = datetime.combine(date.min, t) - datetime.min
        sleep_times.append(t.total_seconds())
    # avg = average(sleep_times)
    avg = sum(sleep_times) / len(sleep_times)
    return strftime("%H:%M:%S", gmtime(avg))


def avg_nap_time():
    fmt = '%H:%M'
    data = get_data()
    nap_time = []
    for i in data:
        time_delta = datetime.strptime(i[2], fmt) - datetime.strptime(i[3], fmt)
        time_delta = str(time_delta).replace('-1 day, ', '')
        time_delta = time_delta.split(':')
        t = time(int(time_delta[0]), int(time_delta[1]))
        t = datetime.combine(date.min, t) - datetime.min
        nap_time.append(t.total_seconds())
    avg = sum(nap_time) / len(nap_time)
    return strftime("%H:%M:%S", gmtime(avg))


avg_nap_time()
# add_data('23:00', '8:00', '7:00')
# add_data('23:30', '7:00', '7:00')
# add_data('23:45', '8:00', '7:00')


def show_data():
    sleeps = get_data()
    for i in sleeps:
        print(f"#{i[0]} {i[5]}\nGodzina pójścia spać: {i[1]}\n"
              f"Godzina pobudki: {i[2]} Godzina pierwszego budzika: {i[3]}\n"
              f"Notatki: {i[4]}\n")


def show_avg_sleep_time():
    avg = avg_sleep_time()
    print(f'Twój średni czas snu to {avg}')


def show_avg_nap_time():
    avg = avg_nap_time()
    print(f'Twój średni czas drzemki to {avg}')


def show_sleep_by_id(id):
    i = get_sleep_by_id(id)
    if i:
        print(f"#{i[0]} {i[5]}\nGodzina pójścia spać: {i[1]}\n"
              f"Godzina pobudki: {i[2]} Godzina pierwszego budzika: {i[3]}\n"
              f"Notatki: {i[4]}\n")
    else:
        print('Brak rekordu o podanym id')


create_table()


def main():

    go_to_menu = "\n\nnaciśnij enter, aby wrócić do menu:"
    sp.call('clear', shell=True)
    sel = input(
        "1.Dodaj sen\n2.Pokaż wszystkie sny\n3.Wyświetl sen\n4.Aktualizuj sen\n"
        "5.Usuń sen\n6.Dodaj archiwalny sen\n7.Średni czas snu\n8.Średni czas drzemki\n0.Wyjdź\n\n")
    if sel == '1':
        sp.call('clear', shell=True)
        go_to_sleep_time = input('Godzina pójścia spać: ')
        wake_up_time = input('Godzina pobudki: ')
        first_alarm = input('O której zamierzałeś wstać: ')
        notes = input('notatki: ')
        add_data(go_to_sleep_time, wake_up_time, first_alarm, notes)
    elif sel == '2':
        sp.call('clear', shell=True)
        show_data()
        input(go_to_menu)
    elif sel == '3':
        sp.call('clear', shell=True)
        id__ = int(input('Wpisz id: '))
        show_sleep_by_id(id__)
        input(go_to_menu)
    elif sel == '4':
        sp.call('clear', shell=True)
        id__ = int(input('Wpisz id: '))
        go_to_sleep_time = input('Godzina pójścia spać: ')
        wake_up_time = input('Godzina pobudki: ')
        first_alarm = input('O której zamierzałeś wstać: ')
        notes = input('notatki: ')
        date = input('data(rrrr-mm-dd): ')
        update_sleep(id__, go_to_sleep_time=go_to_sleep_time, wake_up_time=wake_up_time, first_alarm=first_alarm,
                     notes=notes, date=date)
        input(go_to_menu)
    elif sel == '5':
        sp.call('clear', shell=True)
        id__ = int(input('Wpisz Id: '))
        show_sleep_by_id(id__)
        delete_sleep(id__)
        input(go_to_menu)
    elif sel == '6':
        sp.call('clear', shell=True)
        go_to_sleep_time = input('Godzina pójścia spać: ')
        wake_up_time = input('Godzina pobudki: ')
        first_alarm = input('O której zamierzałeś wstać: ')
        notes = input('notatki: ')
        date = input('data(rrrr-mm-dd): ')
        add_data(go_to_sleep_time, wake_up_time, first_alarm, notes, date)
    elif sel == '7':
        sp.call('clear', shell=True)
        show_avg_sleep_time()
        input(go_to_menu)
    elif sel == '8':
        sp.call('clear', shell=True)
        show_avg_nap_time()
        input(go_to_menu)
    else:
        return 0
    return 1


while main():
    pass
