import cv2
import numpy as np
import matplotlib.pyplot as plt


def canny(img):
    # https://pl.wikipedia.org/wiki/Canny
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    img = cv2.GaussianBlur(img, (0, 0), sigmaX=2, sigmaY=1, borderType=cv2.BORDER_REFLECT)
    img = cv2.Canny(img, 20, 80)
    return img


def region_of_interest(image):
    height = image.shape[0]
    polygons = np.array([[
        # (100, height), (1200, height), (650, 150) # for test2
        # (100, height), (700, height), (400, 200) # for GTA
        # (0, 500), (1100, 500), (500, 200), (800, 200)  # for img from car
        (560, height - 200), (1500, height - 200), (900, 620)  # for video from car
    ]])
    mask = np.zeros_like(image)
    cv2.fillPoly(mask, polygons, 255)
    masked_img = cv2.bitwise_and(image, mask)
    return masked_img


def display_lines(_image, _lines):
    line_image = np.zeros_like(_image)
    if _lines is not None:
        for line in lines:
            x1, y1, x2, y2 = line.reshape(4)
            cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 10)
    return line_image


def make_coordinates(image, line_parameters):
    slope, intercept = line_parameters
    y1 = image.shape[0]
    y2 = int(y1 * (3 / 5))
    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)
    return np.array([x1, y1, x2, y2])


def average_slope_intercept(image, lines):
    left_fit = []
    right_fit = []
    for line in lines:
        x1, y1, x2, y2 = line.reshape(4)
        parameters = np.polyfit((x1, x2), (y1, y2), 11)
        print(parameters)
        slope = parameters[0]
        intercept = parameters[1]
        if slope < 0:
            left_fit.append((slope, intercept))
        else:
            right_fit.append((slope, intercept))
    left_fit_average = np.average(left_fit, axis=0)
    right_fit_average = np.average(right_fit, axis=0)
    left_line = make_coordinates(image, left_fit_average)
    right_line = make_coordinates(image, right_fit_average)
    return np.array([left_line, right_line])


# # frame = cv2.imread('road.PNG')
# frame = cv2.imread('lines.png')
# canny_img = canny(frame)
# cropped_img = region_of_interest(canny_img)
#
# lines = cv2.HoughLinesP(cropped_img, 2, np.pi / 180, 100, np.array([]), minLineLength=40, maxLineGap=10)
# line_img = display_lines(frame, lines)
# combo_img = cv2.addWeighted(frame, .6, line_img, 1, 1)
# # averaged_lines = average_slope_intercept(frame, lines)

#
# plt.imshow(combo_img)
# plt.show()
#
# cv2.imshow('result', combo_img)

cap = cv2.VideoCapture('test_4.mp4')

while cap.isOpened():
    ret, frame = cap.read()

    if ret:
        frame = cv2.flip(frame, -1)

        canny_img = canny(frame)
        cropped_img = region_of_interest(canny_img)

        lines = cv2.HoughLinesP(cropped_img, 2, np.pi / 180, 100, np.array([]), minLineLength=31, maxLineGap=34)
        line_img = display_lines(frame, lines)
        combo_img = cv2.addWeighted(frame, .9, line_img, 1, 1)

        cv2.imshow('result', combo_img)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    else:
        break

cap.release()
cv2.destroyAllWindows()
