import cv2
import numpy as np
import matplotlib.pyplot as plt


def canny(image):
    # https://pl.wikipedia.org/wiki/Canny
    grey = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    blur = cv2.GaussianBlur(grey, (5, 5), 0)
    return cv2.Canny(blur, 40, 80)


def region_of_interest(image):
    height = image.shape[0]
    polygons = np.array([[
        (100, height), (1200, height), (650, 150)  # for test2
        # (100, height), (700, height), (400, 200) # for GTA
        # (0, 500), (1100, 500), (500, 200), (800, 200)  # for img from car
        # (560, height-200), (1500, height-200), (900, 620)  # for video from car
    ]])
    mask = np.zeros_like(image)
    cv2.fillPoly(mask, polygons, 255)
    masked_img = cv2.bitwise_and(image, mask)
    return masked_img


def display_lines(_image, _lines):
    line_image = np.zeros_like(_image)
    if _lines is not None:
        for line in lines:
            x1, y1, x2, y2 = line.reshape(4)
            cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 10)
    return line_image


def make_coordinates(image, line_parameters):
    slope, intercept = line_parameters
    y1 = image.shape[0]
    y2 = int(y1 * (3 / 5))
    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)
    return np.array([x1, y1, x2, y2])


def average_slope_intercept(image, lines):
    left_fit = []
    right_fit = []
    for line in lines:
        x1, y1, x2, y2 = line.reshape(4)
        parameters = np.polyfit((x1, x2), (y1, y2), 1)
        slope = parameters[0]
        intercept = parameters[1]
        if slope < 0:
            left_fit.append((slope, intercept))
        else:
            right_fit.append((slope, intercept))
    left_fit_average = np.average(left_fit, axis=0)
    right_fit_average = np.average(right_fit, axis=0)
    left_line = make_coordinates(image, left_fit_average)
    right_line = make_coordinates(image, right_fit_average)
    return np.array([left_line, right_line])


# frame = cv2.imread('road.PNG')
frame = cv2.imread('test_sun.png')
canny_img = canny(frame)
cropped_img = region_of_interest(canny_img)

for i in range(1, 50):
    lines = cv2.HoughLinesP(cropped_img, 2, np.pi / 180, 100, np.array([]), minLineLength=50 - i, maxLineGap=0 + i)
    print('minLineLength=', 50 - i, 'maxLineGap=', 15 + i)
    line_img = display_lines(frame, lines)
    combo_img = cv2.addWeighted(frame, .6, line_img, 1, 1)
    averaged_lines = average_slope_intercept(frame, lines)

    cv2.imshow('result', combo_img)
    cv2.waitKey(0)
