# Your optional code here
# You can import some modules or create additional functions


def checkio(data: list) -> list:
    return [i for i in data if data.count(i) > 1]


data = [1, 23, 4, 5, 34, 21, 34, 5, 3, 2, 1, 23, 4, 3, 2, 12, 2, 3]
print(checkio(list(data)))

# Some hints
# You can use list.count(element) method for counting.
# Create new list with non-unique elements
# Loop over original list
