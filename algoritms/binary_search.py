import math


def binary_search(arr, el):
    counter = 0
    low = 0
    high = len(arr) - 1
    while low <= high:
        mid = (low + high) // 2
        counter += 1
        if arr[mid] == el:
            print('Element ', el, ' find, at position ', mid, ' at ', counter, ' steps.')
            return mid
        if el < array[mid]:
            high = mid - 1
        else:
            low = mid + 1
    print('There is no', el, 'in this array.')
    return None


array = []
for i in range(0, 1000000):
    if i % 2 == 0 or i % 3 == 0:
        array.append(i)
print('complexity: ', math.ceil(math.log(len(array), 2)))

assert binary_search(array, 0) == 0
assert binary_search(array, 3) == 2
assert binary_search(array, 100) == 67
assert binary_search(array, 500) == 333
assert binary_search(array, 999) == 666
assert binary_search(array, 1000000 - 1) == len(array) - 1
