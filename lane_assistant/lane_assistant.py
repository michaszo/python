import logging
import cv2
import numpy as np
import matplotlib.pyplot as plt


def preparation(img):
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    img = cv2.GaussianBlur(img, (0, 0), sigmaX=2, sigmaY=1, borderType=cv2.BORDER_REFLECT)
    return cv2.Canny(img, 100, 50)


def region_of_interest(image):
    height = image.shape[0]
    polygons = np.array([[
        # (300, height - 250), (1500, height - 250), (1000, 650), (750, 650)
        (300, height - 250), (1500, height - 250), (1000, 700), (750, 700),
    ]])
    mask = np.zeros_like(image)
    cv2.fillPoly(mask, polygons, 255)
    masked_img = cv2.bitwise_and(image, mask)
    return masked_img


def display_lines(_image, _lines):
    line_image = np.zeros_like(_image)
    if _lines is not None:
        for line in lines:
            x1, y1, x2, y2 = line.reshape(4)
            cv2.line(line_image, (x1, y1), (x2, y2), (255, 255, 0), 4)
    return line_image


# def detect_lane(frame):
#     edges = preparation(frame)
#     cropped_edges = region_of_interest(edges)
#     line_segments = detect_line_segments(cropped_edges)
#     lane_lines = average_slope_intercept(frame, line_segments)
#
#     return lane_lines


def average_slope_intercept(frame, line_segments):
    """
    This function combines line segments into one or two lane lines
    If all line slopes are < 0: then we only have detected left lane
    If all line slopes are > 0: then we only have detected right lane
    """
    lane_lines = []
    if line_segments is None:
        logging.info('No line_segment segments detected')
        return lane_lines

    height, width, _ = frame.shape
    left_fit = []
    right_fit = []

    boundary = 1 / 3
    left_region_boundary = width * (1 - boundary)  # left lane line segment should be on left 2/3 of the screen
    right_region_boundary = width * boundary  # right lane line segment should be on left 2/3 of the screen

    for line_segment in line_segments:
        for x1, y1, x2, y2 in line_segment:
            if x1 == x2:
                logging.info('skipping vertical line segment (slope=inf): %s' % line_segment)
                continue
            fit = np.polyfit((x1, x2), (y1, y2), 1)
            slope = fit[0]
            intercept = fit[1]
            if slope < 0:
                if x1 < left_region_boundary and x2 < left_region_boundary:
                    left_fit.append((slope, intercept))
            else:
                if x1 > right_region_boundary and x2 > right_region_boundary:
                    right_fit.append((slope, intercept))

    left_fit_average = np.average(left_fit, axis=0)
    if len(left_fit) > 0:
        lane_lines.append(make_points(frame, left_fit_average))

    right_fit_average = np.average(right_fit, axis=0)
    if len(right_fit) > 0:
        lane_lines.append(make_points(frame, right_fit_average))

    logging.debug('lane lines: %s' % lane_lines)  # [[[316, 720, 484, 432]], [[1009, 720, 718, 432]]]

    # if lane_lines[1][0]:
        # _, _, left_x2, _ = lane_lines[0][0]
        # _, _, right_x2, _ = lane_lines[1][0]
        #
        # mid = int(width / 2)
        # x_offset = (left_x2 + right_x2) / 2 - mid
        # y_offset = int(height / 2)

    return lane_lines


def make_points(frame, line):
    height, width, _ = frame.shape
    slope, intercept = line
    y1 = height  # bottom of the frame
    y2 = int(y1 * 1 / 1.7)  # make points from middle of the frame down

    # bound the coordinates within the frame
    x1 = max(-width, min(2 * width, int((y1 - intercept) / slope)))
    x2 = max(-width, min(2 * width, int((y2 - intercept) / slope)))
    return [[x1, y1, x2, y2]]


def display_lines_a(frame, lines, line_color=(255, 255, 0), line_width=2):
    line_image = np.zeros_like(frame)
    if lines is not None:
        for line in lines:
            for x1, y1, x2, y2 in line:
                cv2.line(line_image, (x1, y1), (x2, y2), line_color, line_width)
    line_image = cv2.addWeighted(frame, 0.8, line_image, 1, 1)
    return line_image


cap = cv2.VideoCapture('test.mp4')
cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

while cap.isOpened():
    ret, frame = cap.read()

    if ret:
        canny = preparation(frame)

        # cv2.imshow('a', canny)
        # cv2.waitKey()

        f = region_of_interest(canny)

        # cv2.imshow('a', f)
        # cv2.waitKey()

        lines = cv2.HoughLinesP(f, 2, np.pi / 180, 100, np.array([]), minLineLength=49, maxLineGap=16)
        line_img = display_lines(frame, lines)

        # cv2.imshow('a', line_img)
        # cv2.waitKey()

        combo_img = cv2.addWeighted(frame, .9, line_img, 1, 1)

        lane_lines = average_slope_intercept(frame, lines)
<<<<<<< HEAD
        cv2.namedWindow('result'Q)
        cv2.resizeWindow('result', 600, 600)
=======

>>>>>>> 226f438c337af4f372599fa0f63af479433dbc5b
        cv2.imshow('result', display_lines_a(frame, lane_lines))

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    else:
        break

cap.release()
cv2.destroyAllWindows()
