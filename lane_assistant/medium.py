import cv2
import numpy as np

frame = cv2.imread('test_sun.png')
img = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
img = cv2.GaussianBlur(img, (0, 0), sigmaX=2, sigmaY=1, borderType=cv2.BORDER_REFLECT)
canny = cv2.Canny(img, 100, 50)


cv2.imshow('', canny)
cv2.waitKey()
