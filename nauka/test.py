binary = 0b11111111110110001011001

print('bin2dec', binary)

b = 27.5400
a = -18.3600

x = a + binary * (b - a) / (2 ** binary.bit_length() - 1)
print(x)

# first
binary = 0b01111111110110001011001
x = a + binary * (b - a) / (2 ** binary.bit_length() - 1)
print('first', x)

# last
binary = 0b11111111110110001011000
x = a + binary * (b - a) / (2 ** binary.bit_length() - 1)
print('last', x)
